PROTOC_FILE_MAIN = ./sgpai.proto
PROTOC_FILE_PROMPT = ./prompt/prompt.proto
PROTOC = protoc
PROTOC_FLAGS = -I . --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative
GIT_ALL = git add . && git commit -m "--" && git push origin main

git:
	@echo "git all"
	$(GIT_ALL)

prompt:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_PROMPT)
	@echo "finishing proto..."

main:
	@echo "create proto..."
	$(PROTOC) $(PROTOC_FLAGS) $(PROTOC_FILE_MAIN)
	@echo "finishing proto..."
	
all: prompt main

.PHONY: all prompt 
